<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/register", name="register")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $email = $user->getEmail();
                    $password = $user->getPassword();
                    $data = [
                        'email' => $email,
                        'passport' => $user->getPassport(),
                        'password' => $password
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();
                    $forSession = $this->getDoctrine()->getRepository('App:User')->checkClientForIdPassword($email,$password);
                    $userHandler->makeSessionUser($forSession);
                    return $this->redirectToRoute("home2");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/",name="home")
     */
    public function homeAction()
    {
        return $this->render('App/home.html.twig');
    }

    /**
     * @Route("/ping", name="ping")
     * @param ApiContext $apiContext
     * @return Response
     */
    public function indexAction(ApiContext $apiContext)
    {
        try {
            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

    /**
     * @Route("/login", name="login")
     * @param UserHandler $user
     * @param Request $request
     * @return Response
     */
    public function authAction(UserHandler $user, Request $request)
    {
        $form = $this->createForm('App\Form\AuthorizationType');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $password = $data['password'];
            $email = $data['email'];

            $findUser = $this->getDoctrine()->getRepository('App:User')->checkClientForIdPassword($email, $password);
            if ($findUser !== null) {
                $user->makeSessionUser($findUser);
                return $this->redirectToRoute('home2');

            } else {

                return new Response('вы не зарегестрированы или ввели не правельный пароль');
            }
        }
        return $this->render('authorization.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/logout" ,name="logout")
     */
    public function logout()
    {
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/home",name="home2")
     * @param UserHandler $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function chek(UserHandler $user)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('login');
        }
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/qwe", name="qwe")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qweAction(ApiContext $apiContext)
    {
        $result = $apiContext->clientExists('some & passport', '123@123.ru');
        return new Response(var_export($result, true));
    }
}
